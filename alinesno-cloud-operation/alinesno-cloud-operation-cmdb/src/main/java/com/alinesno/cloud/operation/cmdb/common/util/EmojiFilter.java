package com.alinesno.cloud.operation.cmdb.common.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import org.apache.commons.lang3.StringUtils;

import com.vdurmont.emoji.EmojiParser;

/**
 * 微信过滤表情
 * 
 * @author hsw
 *
 */
public class EmojiFilter {

	/**
	 * 过滤微信名称
	 */
	public static String filterNickName(String nickname) {
		String filterNickName = null ;
		try {
			filterNickName = filterOffUtf8Mb4(EmojiParser.removeAllEmojis(nickname));
			
			if(StringUtils.isNoneBlank(filterNickName)) {
				String name = new String(filterNickName.getBytes(),"utf-8") ; 
				return name ; 
			}
		} catch (UnsupportedEncodingException e) {
			System.err.println("表情包过滤错误:"+e);
		}
		return "**" ; 
	}

	/**
	 * 过滤掉超过3个字节的UTF8字符
	 * 
	 * @param text
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String filterOffUtf8Mb4(String text) throws UnsupportedEncodingException {
		byte[] bytes = text.getBytes("utf-8");
		ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
		int i = 0;
		while (i < bytes.length) {
			short b = bytes[i];
			if (b > 0) {
				buffer.put(bytes[i++]);
				continue;
			}

			b += 256; // 去掉符号位

			if (((b >> 5) ^ 0x6) == 0) {
				buffer.put(bytes, i, 2);
				i += 2;
			} else if (((b >> 4) ^ 0xE) == 0) {
				buffer.put(bytes, i, 3);
				i += 3;
			} else if (((b >> 3) ^ 0x1E) == 0) {
				i += 4;
			} else if (((b >> 2) ^ 0x3E) == 0) {
				i += 5;
			} else if (((b >> 1) ^ 0x7E) == 0) {
				i += 6;
			} else {
				buffer.put(bytes[i++]);
			}
		}
		buffer.flip();
		return new String(buffer.array(), "utf-8");
	}

	// public static String filterEmoji(String source) {
	// if (source == null) {
	// return source;
	// }
	// Pattern emoji =
	// Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
	// Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
	// Matcher emojiMatcher = emoji.matcher(source);
	// if (emojiMatcher.find()) {
	// source = emojiMatcher.replaceAll("*");
	// return source;
	// }
	// return source;
	// }

	/**
	 * 检测是否有emoji字符
	 * 
	 * @param source
	 *            需要判断的字符串
	 * @return 一旦含有就抛出
	 */
	public static boolean containsEmoji(String source) {
		int len = source.length();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (!notisEmojiCharacter(codePoint)) {
				// 判断确认有表情字符
				return true;
			}
		}
		return false;
	}

	/**
	 * 非emoji表情字符判断
	 * 
	 * @param codePoint
	 * @return
	 */
	private static boolean notisEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD)
				|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
				|| ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}

	/**
	 * 过滤emoji 或者 其他非文字类型的字符
	 * 
	 * @param source
	 *            需要过滤的字符串
	 * @return
	 */
	public static String filterEmoji(String source) {
		if (!containsEmoji(source)) {
			return source;// 如果不包含，直接返回
		}

		StringBuilder buf = null;// 该buf保存非emoji的字符
		int len = source.length();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (notisEmojiCharacter(codePoint)) {
				if (buf == null) {
					buf = new StringBuilder(source.length());
				}
				buf.append(codePoint);
			}
		}

		if (buf == null) {
			return "";// 如果没有找到非emoji的字符，则返回无内容的字符串
		} else {
			if (buf.length() == len) {
				buf = null;
				return source;
			} else {
				return buf.toString();
			}
		}
	}

}