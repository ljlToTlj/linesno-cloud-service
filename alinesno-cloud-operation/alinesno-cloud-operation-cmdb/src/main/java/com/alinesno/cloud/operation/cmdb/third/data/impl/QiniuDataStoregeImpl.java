package com.alinesno.cloud.operation.cmdb.third.data.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.third.data.DataStorege;
import com.alinesno.cloud.operation.cmdb.third.data.QiniuStoreHandler;

/**
 * 七牛数据存储方案
 * @author LuoAnDong
 * @since 2018年2月23日 下午2:37:50
 */
@Service("qiniuDataStoregeImpl")
public class QiniuDataStoregeImpl implements DataStorege{
	
	//日志记录
	private final static Logger logger = LoggerFactory.getLogger(QiniuDataStoregeImpl.class);
	
	@Autowired
	private QiniuStoreHandler qiniuStoreHandler ; 

	@Override
	public String uploadData(String fileLoalAbcPath) {
		logger.debug("file loal abc path = {}" , fileLoalAbcPath);
		String path = null;
		try {
			path = qiniuStoreHandler.upload(fileLoalAbcPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return path ; 
	}

	@Override
	public String downloadData(String fileId) {
		logger.debug("file id = {}" , fileId);
		String path = null;
		try {
			path = qiniuStoreHandler.privateUrl(fileId);
		} catch (IOException e) {
			logger.error("get download url = {}" , e);
		}
		return path ; 
	}

	@Override
	public boolean deleteData(String fileId) {
		return qiniuStoreHandler.deleteData(fileId);
	}

}
