package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import org.springframework.data.domain.Sort;

import com.alinesno.cloud.operation.cmdb.entity.OrderRecordEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface OrderRecordRepository extends BaseJpaRepository<OrderRecordEntity, String> {

	/**
	 * 通过订单号查询订单记录
	 * @param orderId
	 * @return
	 */
	List<OrderRecordEntity> findAllByOrderId(String orderId, Sort by);

}