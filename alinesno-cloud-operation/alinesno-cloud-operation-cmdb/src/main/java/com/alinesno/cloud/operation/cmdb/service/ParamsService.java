package com.alinesno.cloud.operation.cmdb.service;

import java.util.List;

import com.alinesno.cloud.operation.cmdb.entity.ParamsEntity;

/**
 * 参数基础服务
 * @author LuoAnDong
 * @since 2018年8月13日 上午7:18:32
 */
public interface ParamsService  {

	/**
	 * 通过参数名称查询参数
	 * @param name 参数名称
	 * @return 返回参数
	 */
	ParamsEntity findParamByName(String name , String masterCode) ; 
	
	/**
	 * 通过参数名称查询参数
	 * @param name 参数名称
	 * @return 返回参数
	 */
	int findParamByNameToInt(String name , String masterCode) ; 
	
	/**
	 * 添加参数
	 * @param ps
	 * @return
	 */
	Iterable<ParamsEntity> addParamsList(List<ParamsEntity> ps) ;

	/**
	 * 添加参数 
	 * @param params
	 * @return
	 */
	ParamsEntity addParams(ParamsEntity params); 
	 
	/**
	 * 判断是否在运营时间 
	 * @return
	 */
	boolean isRunningTime(String masterCode) ; 

	/**
	 * 判断超市是否在运营时间 
	 * @return
	 */
	boolean isShopRunningTime(String masterCode) ;

	/**
	 * 初始化机房参数
	 * @param masterCode
	 */
	void initSchoolParam(String masterCode);
}
