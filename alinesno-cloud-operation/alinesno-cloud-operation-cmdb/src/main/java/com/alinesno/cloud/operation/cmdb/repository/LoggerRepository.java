package com.alinesno.cloud.operation.cmdb.repository;
import com.alinesno.cloud.operation.cmdb.entity.LoggerEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface LoggerRepository extends BaseJpaRepository<LoggerEntity, String> {

}