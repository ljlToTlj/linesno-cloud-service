package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 评分
 * @author LuoAnDong
 * @since 2018年10月19日 下午10:16:48
 */
public enum ScoreActionEnum {
	
	ACTION_ORDER_FINISH("finish_order" , "完成订单") ,
	ACTION_ORDER_FAST("score_fast" , "快速完成订单") ,
	ACTION_SCORE_BEST("score_best" , "好评") ,
	ACTION_SCORE_BAD("score_bad" , "差评") ; 
	
	public String action ;
	public String text;

	ScoreActionEnum(String action, String text) {
		this.action = action;
		this.text = text;
	}

}
