package com.alinesno.cloud.operation.cmdb.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.entity.ResourceHeadthEntity;
import com.alinesno.cloud.operation.cmdb.entity.ResourceHeadthLoggerEntity;
import com.alinesno.cloud.operation.cmdb.repository.ResourceHeadthLoggerRepository;
import com.alinesno.cloud.operation.cmdb.repository.ResourceHeadthRepository;
import com.alinesno.cloud.operation.cmdb.service.ResourceHeadthService;

/**
 * 通过用户账号获取账户服务 
 * @author LuoAnDong
 * @since 2018年9月23日 上午8:31:57
 */
@Service
@Scope("prototype")
public class ResourceHeadthServiceImpl implements ResourceHeadthService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ResourceHeadthRepository resourceHeadthRepository ; 
	
	@Autowired
	private ResourceHeadthLoggerRepository resourceHeadthLoggerRepository ; 
	
	@Override
	public void updateHeadth(ResourceHeadthEntity entity) {
		logger.debug("更新健康服务:{}" , ReflectionToStringBuilder.toString(entity));
		
		resourceHeadthRepository.save(entity) ; 
	}

	@Override
	public void saveHeadLogger(ResourceHeadthLoggerEntity entity) {
		entity.setLastHeadthTime(new Date());
		resourceHeadthLoggerRepository.save(entity) ; 
	}

	@Override
	public List<ResourceHeadthEntity> findAll() {
		return resourceHeadthRepository.findAll() ;
	}


}
