package com.alinesno.cloud.operation.cmdb.third.sms;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

/**
 * 阿里云短信接口实现
 * 
 * @author LuoAnDong
 * @since 2018年3月3日 上午9:24:08
 */
@Component("aliyunSmsApi")
public class SmsApiImpl implements SmsApi {

	private static final Logger logger = LoggerFactory.getLogger(SmsApiImpl.class);
	
	// 产品名称:云通信短信API产品,开发者无需替换
	String product = "Dysmsapi" ;

	// 产品域名,开发者无需替换
	String domain = "dysmsapi.aliyuncs.com" ;

	// TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
	String accessKeyId = "LTAIn8NNtkQlRutJ";

	String accessKeySecret = "rMv3sLglQNQGhrc4cHBjHySAnOXjSy" ;

	String connectionTimeout = "10000";

	String readTimeout = "10000";

	@Override
	public SmsResponse sendSms(SmsRequest smsRequest) {
		logger.debug("sms request = {}", ToStringBuilder.reflectionToString(smsRequest));

		SmsResponse smsResponse = new SmsResponse() ; 
		try {
			// 可自助调整超时时间
			System.setProperty("sun.net.client.defaultConnectTimeout", connectionTimeout);
			System.setProperty("sun.net.client.defaultReadTimeout", readTimeout);

			// 初始化acsClient,暂不支持region化
			IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
			DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
			IAcsClient acsClient = new DefaultAcsClient(profile);

			// 组装请求对象-具体描述见控制台-文档部分内容
			SendSmsRequest request = new SendSmsRequest();
			
			// 必填:待发送手机号
			request.setPhoneNumbers(smsRequest.getPhone());
			
			// 必填:短信签名-可在短信控制台中找到
			request.setSignName(smsRequest.getSignName());
			
			// 必填:短信模板-可在短信控制台中找到
			request.setTemplateCode(smsRequest.getTemplateCode());
			
			// 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
			request.setTemplateParam(smsRequest.getTemplate());

			// 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
			// request.setSmsUpExtendCode("90997");

			// 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
			request.setOutId(smsRequest.getOutId());

			// hint 此处可能会抛出异常，注意catch
			SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
			
			logger.debug("sendSmsResponse = {}", ToStringBuilder.reflectionToString(sendSmsResponse));
			
			smsResponse.setCode(sendSmsResponse.getCode());
			smsResponse.setDesc(sendSmsResponse.getMessage());
			smsResponse.setBizId(sendSmsResponse.getBizId());
			
			return smsResponse ; 
			
		} catch (Exception e) {
			logger.debug("短信发送失败:{}" , e);
			smsResponse.setCode(String.valueOf(500));
			smsResponse.setDesc("服务器操作失败.");
		}

		return smsResponse;
	}

}