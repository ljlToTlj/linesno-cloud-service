package com.alinesno.cloud.operation.cmdb.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * 邮件实体
 * @author LuoAnDong
 * @since 2018年8月29日 下午7:38:25
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_email")
public class EmailEntity extends BaseEntity {
	
	private String email ; //邮箱
	private String emailOwner ; //邮件主名称
	private String sex ; //性别 
	private int sendTime ; //发送次数
	private int successTime ; //成功次数
	private int failTime ; //失败次数
	private Date lastSendFilaTime ; //最后发送失败时间
	
	@Lob
	private String failDesc ; //异常描述 
	private int rendStatus ; //是否再发送(0需发送|1不再发送)
	
	public String getFailDesc() {
		return failDesc;
	}
	public void setFailDesc(String failDesc) {
		this.failDesc = failDesc;
	}
	public int getRendStatus() {
		return rendStatus;
	}
	public void setRendStatus(int rendStatus) {
		this.rendStatus = rendStatus;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailOwner() {
		return emailOwner;
	}
	public void setEmailOwner(String emailOwner) {
		this.emailOwner = emailOwner;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getSendTime() {
		return sendTime;
	}
	public void setSendTime(int sendTime) {
		this.sendTime = sendTime;
	}
	public int getSuccessTime() {
		return successTime;
	}
	public void setSuccessTime(int successTime) {
		this.successTime = successTime;
	}
	public int getFailTime() {
		return failTime;
	}
	public void setFailTime(int failTime) {
		this.failTime = failTime;
	}
	public Date getLastSendFilaTime() {
		return lastSendFilaTime;
	}
	public void setLastSendFilaTime(Date lastSendFilaTime) {
		this.lastSendFilaTime = lastSendFilaTime;
	}
	
}
