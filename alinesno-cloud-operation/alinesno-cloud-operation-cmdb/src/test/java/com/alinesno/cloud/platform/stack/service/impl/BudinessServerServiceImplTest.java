package com.alinesno.cloud.platform.stack.service.impl;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.operation.cmdb.entity.ServerEntity;
import com.alinesno.cloud.operation.cmdb.service.BudinessServerService;
import com.alinesno.cloud.platform.stack.JUnitBase;

public class BudinessServerServiceImplTest extends JUnitBase {

	@Autowired
	private BudinessServerService budinessServerService ; 
	
	private String masterCode = "10608" ; 

	@Test
	public void testFindByMasterCode() {
		Iterable<ServerEntity> list = budinessServerService.findByMasterCode(masterCode); 
		for(ServerEntity e : list) {
			logger.debug("e = {}" , ToStringBuilder.reflectionToString(e));
		}
	}

}
