package com.alinesno.cloud.base.boot.feign.dto;

import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerTenantDto extends BaseDto {

    /**
     * 租户名称
     */
	private String tenantName;
	
    /**
     * 开始时间
     */
	private Date startTime;
	
    /**
     * 结束时间
     */
	private Date endTime;
	
    /**
     * 租户状态(1正常/0禁止)
     */
	private String tenantStatus;
	
    /**
     * 租户账户
     */
	private String tenantAccount;
	
    /**
     * 租房费用
     */
	private Integer tenantCost;
	
    /**
     * 所属城市
     */
	private String cityId;
	
    /**
     * 所属省份
     */
	private String provinceId;
	
    /**
     * 租户地址
     */
	private String tenantAddress;
	
    /**
     * 租户联系电话
     */
	private String tenantPhone;
	


	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getTenantStatus() {
		return tenantStatus;
	}

	public void setTenantStatus(String tenantStatus) {
		this.tenantStatus = tenantStatus;
	}

	public String getTenantAccount() {
		return tenantAccount;
	}

	public void setTenantAccount(String tenantAccount) {
		this.tenantAccount = tenantAccount;
	}

	public Integer getTenantCost() {
		return tenantCost;
	}

	public void setTenantCost(Integer tenantCost) {
		this.tenantCost = tenantCost;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getTenantAddress() {
		return tenantAddress;
	}

	public void setTenantAddress(String tenantAddress) {
		this.tenantAddress = tenantAddress;
	}

	public String getTenantPhone() {
		return tenantPhone;
	}

	public void setTenantPhone(String tenantPhone) {
		this.tenantPhone = tenantPhone;
	}

}
