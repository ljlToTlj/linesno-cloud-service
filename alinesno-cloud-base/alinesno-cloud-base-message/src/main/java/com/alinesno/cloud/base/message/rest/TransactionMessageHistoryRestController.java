package com.alinesno.cloud.base.message.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.message.entity.TransactionMessageHistoryEntity;
import com.alinesno.cloud.base.message.service.ITransactionMessageHistoryService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:25:27
 */
@Scope("prototype")
@RestController
@RequestMapping("transactionMessageHistory")
public class TransactionMessageHistoryRestController extends BaseRestController<TransactionMessageHistoryEntity , ITransactionMessageHistoryService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(TransactionMessageHistoryRestController.class);

}
