package com.alinesno.cloud.base.storage.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.storage.entity.StorageFileHistoryEntity;
import com.alinesno.cloud.base.storage.repository.StorageFileHistoryRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@NoRepositoryBean
public interface IStorageFileHistoryService extends IBaseService<StorageFileHistoryRepository, StorageFileHistoryEntity, String> {

}
