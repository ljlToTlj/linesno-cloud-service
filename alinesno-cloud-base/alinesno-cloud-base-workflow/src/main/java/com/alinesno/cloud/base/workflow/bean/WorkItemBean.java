package com.alinesno.cloud.base.workflow.bean;

public class WorkItemBean {

	private long processInstID;
	private long activityInstID;

	public long getProcessInstID() {
		return processInstID;
	}

	public void setProcessInstID(long processInstID) {
		this.processInstID = processInstID;
	}

	public long getActivityInstID() {
		return activityInstID;
	}

	public void setActivityInstID(long activityInstID) {
		this.activityInstID = activityInstID;
	}

}
