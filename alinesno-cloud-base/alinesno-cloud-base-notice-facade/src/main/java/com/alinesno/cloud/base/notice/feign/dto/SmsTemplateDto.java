package com.alinesno.cloud.base.notice.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-19 22:02:34
 */
@SuppressWarnings("serial")
public class SmsTemplateDto extends BaseDto {

    /**
     * 模板内容
     */
	private String templateContent;
	
    /**
     * 模板描述
     */
	private String templateDesc;
	
    /**
     * 模板类型
     */
	private String templateType;
	
    /**
     * 模板代码
     */
	private String templateCode;
	


	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateDesc() {
		return templateDesc;
	}

	public void setTemplateDesc(String templateDesc) {
		this.templateDesc = templateDesc;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

}
