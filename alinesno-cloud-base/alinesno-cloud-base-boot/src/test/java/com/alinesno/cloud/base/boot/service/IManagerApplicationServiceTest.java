package com.alinesno.cloud.base.boot.service;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 应用单元测试
 * @author LuoAnDong
 * @since 2019年1月31日 下午4:44:05
 */
public class IManagerApplicationServiceTest extends JUnitBase {

	@Autowired
	private IManagerApplicationService managerApplicationService ; 
	
	@Test
	public void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testSave() {
		
		ManagerApplicationEntity e = new ManagerApplicationEntity() ; 
		e.setApplicationName("基础应用");
		e.setApplicationDesc("基础应用，完成基础的工程结构");
		
		managerApplicationService.save(e) ; 
	}

}
