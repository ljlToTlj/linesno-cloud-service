package com.alinesno.cloud.base.boot.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.base.boot.repository.ContentPostTypeRepository;
import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IContentPostTypeService extends IBaseService<ContentPostTypeRepository, ContentPostTypeEntity, String> {

	List<ContentPostTypeEntity> findAllWithApplication(RestWrapper restWrapper);

}
