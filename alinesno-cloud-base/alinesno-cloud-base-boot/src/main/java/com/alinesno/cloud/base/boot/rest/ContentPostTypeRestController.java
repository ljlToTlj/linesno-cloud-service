package com.alinesno.cloud.base.boot.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.base.boot.service.IContentPostTypeService;
import com.alinesno.cloud.common.core.rest.BaseRestController;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Scope("prototype")
@RestController
@RequestMapping("contentPostType")
public class ContentPostTypeRestController extends BaseRestController<ContentPostTypeEntity , IContentPostTypeService> {

	//日志记录
	private final static Logger log = LoggerFactory.getLogger(ContentPostTypeRestController.class);

	@PostMapping("findAllWithApplication")
	List<ContentPostTypeEntity> findAllWithApplication(@RequestBody RestWrapper restWrapper){
		return feign.findAllWithApplication(restWrapper) ; 
	}

	@PostMapping("findAllByPageable")
	Page<ContentPostTypeEntity> findAllByPageable(Pageable page){
		log.debug("===> page:{}" , JSONObject.toJSON(page));
		return feign.findAll(page) ; 
	}
}
