package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.UserAccountEntity;
import com.alinesno.cloud.base.boot.repository.UserAccountRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p> 基础账户表 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IUserAccountService extends IBaseService<UserAccountRepository, UserAccountEntity, String> {

}
