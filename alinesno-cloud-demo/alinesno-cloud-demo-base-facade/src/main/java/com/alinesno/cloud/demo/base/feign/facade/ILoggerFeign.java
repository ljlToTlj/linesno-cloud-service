package com.alinesno.cloud.demo.base.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.demo.base.feign.dto.LoggerDto;

/**
 * 用户日志服务接口
 * 
 * @author LuoAnDong
 * @since 2018年11月21日 下午2:31:17
 */
@FeignClient(name="alinesno-cloud-demo-base" , url="http://localhost:30002" , path="logger")
public interface ILoggerFeign extends IBaseFeign<LoggerDto> {

}
