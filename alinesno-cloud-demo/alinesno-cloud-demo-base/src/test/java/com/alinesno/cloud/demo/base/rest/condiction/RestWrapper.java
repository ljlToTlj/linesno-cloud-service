package com.alinesno.cloud.demo.base.rest.condiction;

import java.util.Collection;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * 条件构造抽象类，此处借鉴了 <b>mybatis-plus</b>的写法，封装前面的条件，然后转换成Spring Data
 * JPA的Specification方法
 * 
 * @author LuoAnDong
 * @since 2018年12月14日 上午11:48:34
 * @param <T>
 */
public class RestWrapper<T> implements Wrapper<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8590840176306035458L;

	private CriteriaBuilder criteriaBuilder;
	private Predicate predicate ; 
	private Root<T> root ; 

	/**
	 * 转成Spring Data JPA条件查询对象
	 */
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		this.root = root ; 
		this.criteriaBuilder = criteriaBuilder ; 
		this.predicate = criteriaBuilder.conjunction() ; 
		
		return predicate ; 
	}

	@Override
	public Predicate where(boolean condition, String sqlWhere, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate where(String sqlWhere, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate eq(boolean condition, String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate eq(String column, Object params) {
		
		predicate.getExpressions().add(criteriaBuilder.equal(root.get("activityType"), "张三"));

		return null;
	}

	@Override
	public Predicate ne(boolean condition, String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate ne(String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate allEq(boolean condition, Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate allEq(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate gt(boolean condition, String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate gt(String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate ge(boolean condition, String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate ge(String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate lt(boolean condition, String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate lt(String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate le(boolean condition, String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate le(String column, Object params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate and(boolean condition, String sqlAnd, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate and(String sqlAnd, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate andNew(boolean condition, String sqlAnd, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate andNew() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate andNew(String sqlAnd, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate and() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate or() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate or(boolean condition, String sqlOr, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate or(String sqlOr, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate orNew() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate orNew(boolean condition, String sqlOr, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate orNew(String sqlOr, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate groupBy(boolean condition, String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate groupBy(String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate having(boolean condition, String sqlHaving, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate having(String sqlHaving, Object... params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate orderBy(boolean condition, String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate orderBy(String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate orderBy(boolean condition, String columns, boolean isAsc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate orderBy(String columns, boolean isAsc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate like(boolean condition, String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate like(String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notLike(boolean condition, String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notLike(String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate like(boolean condition, String column, String value, CondictionLike type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate like(String column, String value, CondictionLike type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notLike(boolean condition, String column, String value, CondictionLike type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notLike(String column, String value, CondictionLike type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate isNotNull(boolean condition, String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate isNotNull(String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate isNull(boolean condition, String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate isNull(String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate exists(boolean condition, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate exists(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notExists(boolean condition, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notExists(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate in(boolean condition, String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate in(String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notIn(boolean condition, String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notIn(String column, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate in(boolean condition, String column, Collection<?> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate in(String column, Collection<?> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notIn(boolean condition, String column, Collection<?> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notIn(String column, Collection<?> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate in(boolean condition, String column, Object[] value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate in(String column, Object[] value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notIn(boolean condition, String column, Object... value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notIn(String column, Object... value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate between(boolean condition, String column, Object val1, Object val2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate between(String column, Object val1, Object val2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notBetween(boolean condition, String column, Object val1, Object val2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Predicate notBetween(String column, Object val1, Object val2) {
		// TODO Auto-generated method stub
		return null;
	}

	public CriteriaBuilder getCriteriaBuilder() {
		return criteriaBuilder;
	}

	public void setCriteriaBuilder(CriteriaBuilder criteriaBuilder) {
		this.criteriaBuilder = criteriaBuilder;
	}

}
