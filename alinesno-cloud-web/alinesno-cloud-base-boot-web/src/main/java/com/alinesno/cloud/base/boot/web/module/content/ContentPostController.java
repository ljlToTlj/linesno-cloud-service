package com.alinesno.cloud.base.boot.web.module.content;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ContentPostsDto;
import com.alinesno.cloud.base.boot.feign.facade.ContentPostsFeigin;
import com.alinesno.cloud.base.boot.web.module.organization.AccountController;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;

/**
 * 内容管理
 * @author LuoAnDong
 * @since 2019年4月4日 下午1:23:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/content")
public class ContentPostController  extends FeignMethodController<ContentPostsDto , ContentPostsFeigin> {

	private static final Logger log = LoggerFactory.getLogger(AccountController.class) ; 

	/**
	 * 保存新对象 
	 * @param model
	 * @param contentPostsDto
	 * @return
	 */
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model , HttpServletRequest request, ContentPostsDto contentPostsDto) {
		contentPostsDto.setPostAuthor(CurrentAccountSession.get(request).getId());  
		contentPostsDto = feign.save(contentPostsDto) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/add")
    public void add(Model model , HttpServletRequest request){
		List<ContentPostsDto> codeTypes = feign.findAll() ; 
		model.addAttribute("codeTypes", codeTypes) ; 
    }
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@Override
	public void modify(HttpServletRequest request ,Model model , String id){
		Assert.hasLength(id , "主键不能为空.");
		ContentPostsDto code = feign.getOne(id) ; 
	
		List<ContentPostsDto> codeTypes = feign.findAll() ; 
		model.addAttribute("codeTypes", codeTypes) ; 
		model.addAttribute("bean", code) ; 
    }
	
	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId){
		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
		if(rowsId != null && rowsId.length > 0) {
			feign.deleteByIds(rowsId); 
		}
		return ResponseGenerator.ok(null) ; 
    }

	@TranslateCode(value="[{hasStatus:has_status}]" , plugin = "contentTranslatePlugin")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }

//	private DatatablesPageBean toPage(Model model, ContentPostsFeigin feign, DatatablesPageBean page) {
//		RestWrapper restWrapper = new RestWrapper() ; 
//		RestPage<ManagerCodeDto> pageable = new RestPage<ManagerCodeDto>(page.getStart() / page.getLength(), page.getLength()) ; 
//		restWrapper.setPageable(pageable); 
//		restWrapper.builderCondition(page.getCondition()) ; 
//		
//		RestPage<ContentPostsDto> pageableResult = feign.findAllByWrapperAndPageable(restWrapper) ; 
//		
//		DatatablesPageBean p = new DatatablesPageBean();
//		p.setData(pageableResult.getContent());
//		p.setDraw(page.getDraw());
//		p.setRecordsFiltered((int) pageableResult.getTotalElements());
//		p.setRecordsTotal((int) pageableResult.getTotalElements());
//		return p ;
//	}

//	@RequestMapping("/typeList")
//    public void typeList(){
//    }
//	
//	/**
//	 * 保存新对象 
//	 * @param model
//	 * @param contentPostsDto
//	 * @return
//	 */
//	@FormToken(remove=true)
//	@ResponseBody
//	@PostMapping("/saveType")
//	public ResponseBean saveType(Model model , HttpServletRequest request, ContentPostTypeDto managerCodeTypeDto) {
//		managerCodeTypeDto = contentPostTypeFeigin.save(managerCodeTypeDto) ; 
//		return ResponseGenerator.ok(null) ; 	
//	}
//	
//	/**
//	 * 代码管理查询功能  
//	 * @return
//	 */
//	@FormToken(save=true)
//	@GetMapping("/addType")
//    public void addType(Model model , HttpServletRequest request){
//    }
//	
//	/**
//	 * 代码管理查询功能  
//	 * @return
//	 */
//	@FormToken(save=true)
//	@GetMapping("/modifyType")
//    public void modifyType(Model model , HttpServletRequest request , String id){
//		Assert.hasLength(id , "主键不能为空.");
//		
//		ContentPostTypeDto code = contentPostTypeFeigin.getOne(id) ; 
//		model.addAttribute("bean", code) ; 
//    }
//	
//	/**
//	 * 保存
//	 * @param model
//	 * @param request
//	 * @param managerApplicationDto
//	 * @return
//	 */
//	@FormToken(remove=true)
//	@ResponseBody
//	@PostMapping("/updateType")
//	public ResponseBean updateType(Model model , HttpServletRequest request, ContentPostTypeDto managerCodeTypeDto) {
//		
//		ContentPostTypeDto oldBean = contentPostTypeFeigin.getOne(managerCodeTypeDto.getId()) ; 
//		BeanUtil.copyProperties(managerCodeTypeDto, oldBean , CopyOptions.create().setIgnoreNullValue(true));
//		
//		managerCodeTypeDto = contentPostTypeFeigin.save(oldBean) ; 
//		return ResponseGenerator.ok(null) ; 	
//	}
//	
//
//	@TranslateCode("[{hasStatus:has_status}]")
//	@ResponseBody
//	@RequestMapping("/typeDatatables")
//    public DatatablesPageBean typeDatatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
//		return this.toTypePage(model, contentPostTypeFeigin , page) ;
//    }
//
//	private DatatablesPageBean toTypePage(Model model, ContentPostTypeFeigin contentPostTypeFeigin , DatatablesPageBean page) {
//		
//		// ----------------------- 兼容bootstrap table_start ---------------
//		if(page.isBootstrapTable()) {
//			page.setStart(page.getPageNum()-1);
//			page.setLength(page.getPageSize());
//		}
//		// ----------------------- 兼容bootstrap table_end ---------------
//		
//		RestWrapper restWrapper = new RestWrapper() ; 
//		restWrapper.setPageNumber(page.getStart());
//		restWrapper.setPageSize(page.getLength());
//		
//		restWrapper.builderCondition(page.getCondition()) ; 
//
//		Page<ContentPostTypeDto> pR = contentPostTypeFeigin.findAllByWrapperAndPageable(restWrapper) ; 
//		
//		if(page.isBootstrapTable()) {
//			// ----------------------- 兼容bootstrap table_start ---------------
//			page.setRows(pR.getContent());
//			page.setTotal((int) pR.getTotalElements());
//			page.setCode(HttpStatus.OK.value()); 
//			// ----------------------- 兼容bootstrap table_end ---------------
//		} else {
//			page.setData(pR.getContent());
//			page.setDraw(page.getDraw());
//			page.setRecordsFiltered((int) pR.getTotalElements());
//			page.setRecordsTotal((int) pR.getTotalElements());
//		}
//		
//		return page ;
//		
//	}
//	
//	/**
//	 * 删除
//	 */
//	@ResponseBody
//	@PostMapping("/deleteType")
//    public ResponseBean deleteType(@RequestParam(value = "rowsId[]") String[] rowsId){
//		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
//		if(rowsId != null && rowsId.length > 0) {
//			contentPostTypeFeigin.deleteByIds(rowsId); 
//		}
//		return ResponseGenerator.ok(null) ; 
//    }
//
//	/**
//	 * 删除
//	 */
//	@ResponseBody
//	@PostMapping("/allType")
//    public List<ContentPostTypeDto> allType(){
//		RestWrapper restWrapper = new RestWrapper() ; 
//		List<ContentPostTypeDto> types = contentPostTypeFeigin.findAllWithApplication(restWrapper) ; 
//		return types ; 
//    }
	
	
}
